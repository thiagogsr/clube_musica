var caminho = "/clientes/clube";

$(".form").validate();

$(".mensagens p").click(function() {
	$(this).fadeOut("fast", function() {
		$(this).remove();
	});
});

$.extend({
	aplicarMoeda: function() {
		$("input.moeda").priceFormat({prefix: "R$ ", centsSeparator: ",", thousandsSeparator: "."});
		return this;
	},
	aplicarData: function() {
		$("input.data").datepicker({altFormat:'dd/mm/yyyy', 'changeMonth':true, 'changeYear':true, 'yearRange':'1920:+10'}).mask("99/99/9999");
		return this;
	},
	aplicarTelefone: function() {
		$("input.telefone").mask("(99) 9999-9999");
		return this;
	}
});

$.aplicarMoeda();
$.aplicarData();
$.aplicarTelefone();