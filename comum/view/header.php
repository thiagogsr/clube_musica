<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Clube da Música</title>

<link rel="stylesheet" href="<?php echo Config::$root; ?>/comum/css/estilo.css" />
<link rel="stylesheet" href="<?php echo Config::$root; ?>/comum/css/redmond/jquery-ui-1.8.18.custom.css" />

</head>
<body>

<div id="header">
	<div class="wrap">
		<h1 class="logo"><a href="<?php echo Config::$root; ?>/index.php" title="Clube de Música"><span>Clube de Música</span></a></h1>
		<div class="data corner">Aracaju, <?php echo date("d/m/Y H:i"); ?></div>
	</div>
</div>

<div id="nav">
	<div class="wrap">
		<ul class="corner">
			<li><a href="<?php echo Config::$root; ?>/titulo/">Título</a></li>
			<li><a href="<?php echo Config::$root; ?>/locacao/ranking/">Ranking</a></li>
			<li><a href="<?php echo Config::$root; ?>/tipotitulo/">Tipo de Título</a></li>
			<li><a href="<?php echo Config::$root; ?>/gravadora/">Gravadora</a></li>
			<li><a href="<?php echo Config::$root; ?>/artista/">Artista</a></li>
			<li><a href="<?php echo Config::$root; ?>/cliente/">Cliente</a></li>
			<li><a href="<?php echo Config::$root; ?>/doador/">Doador</a></li>
			<li><a href="<?php echo Config::$root; ?>/fornecedor/">Fornecedor</a></li>
			<li><a href="<?php echo Config::$root; ?>/doacao/">Doação</a></li>
			<li><a href="<?php echo Config::$root; ?>/locacao/">Locação</a></li>
			<li><a href="<?php echo Config::$root; ?>/locacao/atrasos/">Em Atraso</a></li>
			<li><a href="<?php echo Config::$root; ?>/locacao/emprestados/">Em empréstimo</a></li>
			<li><a href="<?php echo Config::$root; ?>/compra/">Compra</a></li>
		</ul>
	</div>
</div>