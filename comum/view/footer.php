<div id="footer">
	<div class="wrap">
		<p>Copyright &copy; 2012 - Thiago Guimarães. Clube da Música.</p>
	</div>
</div>

<script src="<?php echo Config::$root; ?>/comum/js/jquery.js"></script>
<script src="<?php echo Config::$root; ?>/comum/js/jqueryui.js"></script>
<script src="<?php echo Config::$root; ?>/comum/js/datepicker-pt-BR.js"></script>
<script src="<?php echo Config::$root; ?>/comum/js/validate.js"></script>
<script src="<?php echo Config::$root; ?>/comum/js/price_format.js"></script>
<script src="<?php echo Config::$root; ?>/comum/js/maskedinput.js"></script>
<script src="<?php echo Config::$root; ?>/comum/js/comum.js"></script>
<?php
if(!empty($Ascript)) {
	foreach ($Ascript as $script) {
		echo "<script src='" . Config::$root . $script . "'></script>\n";
	}
}
?>

</body>
</html>