<?php
include_once "locacao/model/Locacao.php";

class LocacaoClass extends LocacaoModel {

	public $id;
	public $cliente_id;
	public $data;

	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->cliente_id = $PAdados['cliente_id'];
			$this->data = $PAdados['data'];
		}
	}

	public function edit() {
		$Alocacao = parent::editar($this->id);
		$this->id = $Alocacao['id'];
		$this->cliente_id = $Alocacao['cliente_id'];
		$this->data = $Alocacao['data'];
	}
	
	public function consultar($PAdados) {
		$PAdados['filtro_dia'] = Config::converterData($PAdados['filtro_dia']);
		$PAdados['filtro_data_inicial'] = Config::converterData($PAdados['filtro_data_inicial']);
		$PAdados['filtro_data_final'] = Config::converterData($PAdados['filtro_data_final']);
		return parent::consultar($PAdados);
	}

	public function cadastrar($PAdados) {
		$PAdados['data'] = Config::converterData($PAdados['data']);
		return parent::cadastrar($PAdados);
	}

	public function atualizar($PAdados) {
		$PAdados['data'] = Config::converterData($PAdados['data']);
		return parent::atualizar($PAdados);
	}

}