<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "locacao/classes/LocacaoClass.php";
include_once "cliente/classes/ClienteClass.php";
include_once "titulo/classes/TituloClass.php";
include_once "locacao_item/classes/LocacaoItemClass.php";

switch ($PAdados['acao']) {
	case "novo":
		LocacaoController::novo($PAdados);
		break;
	case "cadastrar":
		LocacaoController::cadastrar($PAdados);
		break;
	case "editar":
		LocacaoController::editar($PAdados);
		break;
	case "atualizar":
		LocacaoController::atualizar($PAdados);
		break;
	case "novoitem":
		LocacaoController::novoitem($PAdados);
		break;
	case "ranking":
		LocacaoController::ranking($PAdados);
		break;
	case "atrasos":
		LocacaoController::atrasos($PAdados);
		break;
	case "emprestados":
		LocacaoController::emprestados($PAdados);
		break;
	default:
		LocacaoController::consultar($PAdados);
		break;
}

class LocacaoController {
	
	public function consultar($PAdados) {
		$Alocacao = LocacaoClass::consultar($PAdados);
		$Acliente = ClienteClass::consultar(array("filtro_ativo" => true));
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		$Acliente = ClienteClass::consultar(array("filtro_ativo" => true));
		$Atitulo = TituloClass::consultar(array("filtro_ativo" => true, "filtro_quantidade" => true));
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['status'] = LocacaoClass::cadastrar($PAdados);
		
		if(!empty($PAdados['titulo_id'])) {
			$Adados['locacao_id'] = mysql_insert_id();
			foreach ($PAdados['titulo_id'] as $key => $value) {
				$Adados['titulo_id'] = $value;
				$Adados['valor'] = $PAdados['valor'][$key];
				$Adados['data_devolucao'] = $PAdados['data_devolucao'][$key];
				$Adados['multa'] = $PAdados['multa'][$key];
				LocacaoItemClass::cadastrar($Adados);
				
				$objTituloClass = new TituloClass(array("id" => $value));
				$objTituloClass->edit();
				$quantidade = $objTituloClass->quantidade - 1;
				$objTituloClass->atualizarQuantidade(array("id" => $value, "quantidade" => $quantidade));
			}
		}
		
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objLocacaoClass = new LocacaoClass($PAdados);
		$objLocacaoClass->edit();
		
		$Acliente = ClienteClass::consultar(array("filtro_ativo" => true));
		$Atitulo = TituloClass::consultar(array("filtro_ativo" => true, "filtro_quantidade" => true));
		
		$Aitens = LocacaoItemClass::consultar($PAdados);
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = LocacaoClass::atualizar($PAdados);
		
		LocacaoItemClass::apagar($PAdados);
		if(!empty($PAdados['titulo_id'])) {
			$Adados['locacao_id'] = $PAdados['id'];
			foreach ($PAdados['titulo_id'] as $key => $value) {
				$Adados['titulo_id'] = $value;
				$Adados['valor'] = $PAdados['valor'][$key];
				$Adados['data_devolucao'] = $PAdados['data_devolucao'][$key];
				$Adados['multa'] = $PAdados['multa'][$key];
				$Adados['data_devolvido'] = $PAdados['data_devolvido'][$key];
				LocacaoItemClass::cadastrar($Adados);
			}
		}
		
		self::consultar($PAdados);
	}
	
	public function novoitem() {
		$Atitulo = TituloClass::consultar(array("filtro_ativo" => true));
		include_once "../view/novoitem.php";
	}
	
	public function ranking() {
		$Alocacao = LocacaoClass::consultarRanking();
		include_once "../view/ranking.php";
	}
	
	public function atrasos($PAdados) {
		$PAdados['em-atraso'] = true;
		$Alocacao = LocacaoClass::consultarRelatorio($PAdados);
		include_once "../view/atrasos.php";
	}
	
	public function emprestados($PAdados) {
		$Alocacao = LocacaoClass::consultarRelatorio($PAdados);
		include_once "../view/emprestados.php";
	}

}