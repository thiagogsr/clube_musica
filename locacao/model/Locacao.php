<?php
class LocacaoModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, cliente_id, data,
				(select nome from cliente where id = loc.cliente_id) AS cliente_nome,
				(select count(*) from locacao_item where locacao_id = loc.id) AS itens
				FROM locacao loc
				WHERE 1";
		
		if($PAdados['filtro_cliente_id']) {
			$sql .= " AND cliente_id = {$PAdados['filtro_cliente_id']} ";
		}
		
		if($PAdados['filtro_dia']) {
			$sql .= " AND data = '{$PAdados['filtro_dia']}' ";
		}
		
		if($PAdados['filtro_data_inicial']) {
			$sql .= " AND data >= '{$PAdados['filtro_data_inicial']}' ";
		}
		
		if($PAdados['filtro_data_final']) {
			$sql .= " AND data <= '{$PAdados['filtro_data_final']}' ";
		}
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['cliente_id'] = $linha['cliente_id'];
			$Adados[$i]['cliente_nome'] = $linha['cliente_nome'];
			$Adados[$i]['itens'] = $linha['itens'];
			$Adados[$i]['data'] = $linha['data'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function consultarRanking() {
		$sql = "SELECT COUNT(*) AS quantidade,
				(select nome from cliente where id = loc.cliente_id) AS cliente_nome
				FROM locacao loc
				GROUP BY cliente_id
				ORDER BY quantidade DESC";
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['cliente_nome'] = $linha['cliente_nome'];
			$Adados[$i]['quantidade'] = $linha['quantidade'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function consultarRelatorio($PAdados) {
		$sql = "SELECT loc.id, cli.nome AS cliente_nome, tit.nome AS titulo_nome, lol.data_devolucao, lol.multa
				FROM locacao loc
				INNER JOIN locacao_item lol ON loc.id = lol.locacao_id
				INNER JOIN cliente cli ON cli.id = loc.cliente_id
				INNER JOIN titulo tit ON tit.id = lol.titulo_id
				WHERE data_devolvido IS NULL";
	
		if(!empty($PAdados['em-atraso'])) {
			$sql .= " AND data_devolucao < NOW()";
		}
		
		$sql .= " ORDER BY data_devolucao ASC";
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['cliente_nome'] = $linha['cliente_nome'];
			$Adados[$i]['titulo_nome'] = $linha['titulo_nome'];
			$Adados[$i]['data_devolucao'] = $linha['data_devolucao'];
			$Adados[$i]['multa'] = $linha['multa'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO locacao 
				VALUES (
					NULL,
					{$PAdados['cliente_id']},
					'{$PAdados['data']}'
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, cliente_id, data
				FROM locacao
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE locacao 
				SET 
					cliente_id = {$PAdados['cliente_id']},
					data = '{$PAdados['data']}'
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}