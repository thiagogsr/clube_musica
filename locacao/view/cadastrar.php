<?php include_once "comum/view/header.php"; ?>

<?php $Ascript = array("/locacao/view/js/index.js"); ?>

<div id="content">
	<div class="wrap">
		<h2>Cadastrar Locação</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/cadastrar/"; ?>" method="post">
			<div>
				<label>Cliente</label>
				<select name="cliente_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Acliente)) {
						foreach ($Acliente as $linha) {
							echo "<option value='{$linha['id']}'>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Data</label>
				<input type="text" name="data" class="required data" value="<?php echo date("d/m/Y"); ?>" />
			</div>
			
			<table width="100%" id="panel-item">
				<thead>
					<tr>
						<th>Título</th>
						<th>Valor</th>
						<th>Data de devolução</th>
						<th>Multa</th>
						<th class="center">Ação</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<select name="titulo_id[]" class="required">
								<option value="">Selecione uma opção</option>
								<?php 
									if (!empty($Atitulo)) {
										foreach ($Atitulo as $linha) {
											echo "<option value='{$linha['id']}'>{$linha['nome']}</option>";
										}
									}
								?>
							</select>
						</td>
						<td><input type="text" name="valor[]" class="required moeda" /></td>
						<td><input type="text" name="data_devolucao[]" class="required data" /></td>
						<td><input type="text" name="multa[]" class="required moeda" /></td>
						<td align="center"><a href="#" class="adicionar" title="Adicionar item"><span>Adicionar item</span></a></td>
					</tr>
				</tbody>
			</table>
			
			<div class="submit">
				<input type="submit" value="Cadastrar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>