<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Ranking</h2>
		
		<table width="100%">
			<thead>
				<tr>
					<th>Cliente</th>
					<th>Quantidade</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(!empty($Alocacao)) {
					foreach ($Alocacao as $linha) {
						echo "<tr>";
						echo "<td>{$linha['cliente_nome']}</td>";
						echo "<td>{$linha['quantidade']}</td>";
						echo "</tr>";
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>