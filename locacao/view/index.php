<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Locações <span>(<a href="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/novo"; ?>" title="Novo">Novo</a>)</span></h2>
		
		<div class="mensagens">
			<?php 
			if($PAdados['acao'] == "cadastrar" && $PAdados['status']) { echo "<p>Cadastrado com sucesso</p>"; }
			if($PAdados['acao'] == "atualizar" && $PAdados['status']) { echo "<p>Atualizado com sucesso</p>"; }
			if($PAdados['acao'] == "cadastrar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao cadastrar</p>"; }
			if($PAdados['acao'] == "atualizar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao atualizar</p>"; }
			?>
		</div>
		
		<div class="filtro">
			<form class="frm-filtro" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/"; ?>" method="post">
				<div>
					<label>Cliente</label>
					<select name="filtro_cliente_id">
						<option value="">Selecione uma opção</option>
						<?php 
							if(!empty($Acliente)) {
								foreach ($Acliente as $linha) {
									$selected = $linha['id'] == $PAdados['filtro_cliente_id'] ? "selected='selected'" : "";
									echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
								}
							}
						?>
					</select>
				</div>
				
				<div>
					<label>Dia</label>
					<input type="text" name="filtro_dia" class="data" value="<?php echo $PAdados['filtro_dia'] ? $PAdados['filtro_dia'] : ""; ?>" />
				</div>
				
				<div>
					<label>Data inicial</label>
					<input type="text" name="filtro_data_inicial" class="data" value="<?php echo $PAdados['filtro_data_inicial'] ? $PAdados['filtro_data_inicial'] : ""; ?>" />
				</div>
				
				<div>
					<label>Data final</label>
					<input type="text" name="filtro_data_final" class="data" value="<?php echo $PAdados['filtro_data_final'] ? $PAdados['filtro_data_final'] : ""; ?>" />
				</div>
				
				<div class="submit">
				<input type="submit" value="Filtrar" />
			</div>
			</form>
		</div>
		
		<table width="100%">
			<thead>
				<tr>
					<th>Cliente</th>
					<th>Itens</th>
					<th>Data</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(!empty($Alocacao)) {
					foreach ($Alocacao as $linha) {
						echo "<tr>";
						echo "<td><a href='" . Config::$root . "/{$PAdados['modulo']}/editar/{$linha['id']}' title='Editar'>{$linha['cliente_nome']}</a></td>";
						echo "<td>{$linha['itens']}</td>";
						echo "<td>" . Config::converterData($linha['data'], 2) . "</td>";
						echo "</tr>";
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>