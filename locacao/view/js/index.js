$(document).ready(function() {
	
	$(".adicionar").click(function(e) {
		e.preventDefault();
		
		$.get(caminho + "/locacao/novoitem/", function(data) {
			$("#panel-item tbody").append(data);
			$.aplicarMoeda();
			$.aplicarData();
		});
	});
	
	$(document).on("click", ".remover", function(e) {
		e.preventDefault();
		$(this).parent().parent().remove();
	});
	
});