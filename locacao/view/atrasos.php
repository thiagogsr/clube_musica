<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Locações em atraso</h2>
		
		<table width="100%">
			<thead>
				<tr>
					<th>Título</th>
					<th>Cliente</th>
					<th>Data de devolução</th>
					<th>Multa</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(!empty($Alocacao)) {
					foreach ($Alocacao as $linha) {
						echo "<tr>";
						echo "<td><a href='" . Config::$root . "/{$PAdados['modulo']}/editar/{$linha['id']}' title='Editar'>{$linha['titulo_nome']}</a></td>";
						echo "<td><a href='" . Config::$root . "/{$PAdados['modulo']}/editar/{$linha['id']}' title='Editar'>{$linha['cliente_nome']}</a></td>";
						echo "<td>" . Config::converterData($linha['data_devolucao'], 2) . "</td>";
						echo "<td>R$ " . Config::converterMoeda($linha['multa'], 2) . "</td>";
						echo "</tr>";
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>