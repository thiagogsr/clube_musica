<?php include_once "comum/view/header.php"; ?>

<?php $Ascript = array("/locacao/view/js/index.js"); ?>

<div id="content">
	<div class="wrap">
		<h2>Editar Locação</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/atualizar/" . $PAdados['id']; ?>" method="post">
			<div>
				<label>Cliente</label>
				<select name="cliente_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Acliente)) {
						foreach ($Acliente as $linha) {
							$selected = $linha['id'] == $objLocacaoClass->cliente_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Data</label>
				<input type="text" name="data" class="required data" value="<?php echo Config::converterData($objLocacaoClass->data, 2); ?>" />
			</div>
			
			<table width="100%" id="panel-item">
				<thead>
					<tr>
						<th>Título</th>
						<th>Valor</th>
						<th>Data de devolução</th>
						<th>Multa</th>
						<th>Data devolvido</th>
					</tr>
				</thead>
				<tbody>
					<?php 
					if(!empty($Aitens)) {
						foreach ($Aitens as $linha) {
							echo "<tr>";
							echo "<td><select name='titulo_id[]'>";
							if (!empty($Atitulo)) {
								foreach ($Atitulo as $titulo) {
									$selected = $titulo['id'] == $linha['titulo_id'] ? "selected='selected'" : "";
									echo "<option value='{$titulo['id']}' $selected>{$titulo['nome']}</option>";
								}
							}
							echo "</select></td>";
							echo "<td><input type='text' name='valor[]' class='moeda' value='" . Config::converterMoeda($linha['valor'], 2) . "' /></td>";
							echo "<td><input type='text' name='data_devolucao[]' class='data' value='" . Config::converterData($linha['data_devolucao'], 2) . "' /></td>";
							echo "<td><input type='text' name='multa[]' class='moeda' value='" . Config::converterMoeda($linha['multa'], 2) . "' /></td>";
							echo "<td><input type='text' name='data_devolvido[]' class='data' value='" . (!empty($linha['data_devolvido']) ? Config::converterData($linha['data_devolvido'], 2) : "") . "' /></td>";
							echo "</tr>\n";
						}
					}
					?>
				</tbody>
			</table>
			
			<div class="submit">
				<input type="submit" value="Atualizar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>