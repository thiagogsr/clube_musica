<?php
include_once "artista/model/Artista.php";

class ArtistaClass extends ArtistaModel {
	
	public $id;
	public $gravadora_id;
	public $nome;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->gravadora_id = $PAdados['gravadora_id'];
			$this->nome = $PAdados['nome'];
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Aartista = parent::editar($this->id);
		$this->id = $Aartista['id'];
		$this->gravadora_id = $Aartista['gravadora_id'];
		$this->nome = $Aartista['nome'];
		$this->cadastro = $Aartista['cadastro'];
		$this->status = $Aartista['status'];
	}
	
}