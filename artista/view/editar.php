<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Editar Artista</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/atualizar/" . $PAdados['id']; ?>" method="post">
			<div>
				<label>Nome</label>
				<input type="text" name="nome" class="required" value="<?php echo $objArtistaClass->nome; ?>" />
			</div>
			
			<div>
				<label>Gravadora</label>
				<select name="gravadora_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Agravadora)) {
						foreach ($Agravadora as $linha) {
							$selected = $linha['id'] == $objArtistaClass->gravadora_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Ativo</label>
				<select name="status">
					<option value="1" <?php echo $objArtistaClass->status == 1 ? "selected='selected'" : ""; ?>>Sim</option>
					<option value="0" <?php echo $objArtistaClass->status == 0 ? "selected='selected'" : ""; ?>>Não</option>
				</select>
			</div>
			
			<div class="submit">
				<input type="submit" value="Atualizar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>