<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "artista/classes/ArtistaClass.php";
include_once "gravadora/classes/GravadoraClass.php";

switch ($PAdados['acao']) {
	case "novo":
		ArtistaController::novo($PAdados);
		break;
	case "cadastrar":
		ArtistaController::cadastrar($PAdados);
		break;
	case "editar":
		ArtistaController::editar($PAdados);
		break;
	case "atualizar":
		ArtistaController::atualizar($PAdados);
		break;
	case "ativar":
		ArtistaController::ativar($PAdados);
		break;
	case "inativar":
		ArtistaController::inativar($PAdados);
		break;
	default:
		ArtistaController::consultar($PAdados);
		break;
}

class ArtistaController {
	
	public function consultar($PAdados) {
		$Aartista = ArtistaClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		$Agravadora = GravadoraClass::consultar(array("filtro_ativo" => true));
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = ArtistaClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objArtistaClass = new ArtistaClass($PAdados);
		$objArtistaClass->edit();
		
		$Agravadora = GravadoraClass::consultar(array("filtro_ativo" => true));
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = ArtistaClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = ArtistaClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = ArtistaClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}