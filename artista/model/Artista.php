<?php
class ArtistaModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, gravadora_id, nome, cadastro, status,
				(select nome from gravadora where id = art.gravadora_id) AS gravadora_nome
				FROM artista art";
		
		if($PAdados['filtro_ativo']) {
			$sql .= " WHERE status = 1 ";
		}
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['gravadora_id'] = $linha['gravadora_id'];
			$Adados[$i]['gravadora_nome'] = $linha['gravadora_nome'];
			$Adados[$i]['nome'] = $linha['nome'];
			$Adados[$i]['cadastro'] = $linha['cadastro'];
			$Adados[$i]['status'] = $linha['status'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO artista 
				VALUES (
					NULL,
					'{$PAdados['gravadora_id']}',
					'{$PAdados['nome']}',
					'{$PAdados['cadastro']}',
					{$PAdados['status']}
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, gravadora_id, nome, cadastro, status
				FROM artista
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE artista 
				SET 
					gravadora_id = '{$PAdados['gravadora_id']}',
					nome = '{$PAdados['nome']}',
					status = {$PAdados['status']}
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function ativar($PAdados) {
		$sql = "UPDATE artista SET status = 1 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function inativar($PAdados) {
		$sql = "UPDATE artista SET status = 0 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}