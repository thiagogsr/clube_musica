<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Cadastrar Tipo de Título</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/cadastrar/"; ?>" method="post">
			<div>
				<label>Nome</label>
				<input type="text" name="nome" class="required" />
			</div>
			
			<div>
				<label>Ativo</label>
				<select name="status">
					<option value="1" selected="selected">Sim</option>
					<option value="0">Não</option>
				</select>
			</div>
			
			<div class="submit">
				<input type="submit" value="Cadastrar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>