<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "tipotitulo/classes/TipoTituloClass.php";

switch ($PAdados['acao']) {
	case "novo":
		TipoTituloController::novo($PAdados);
		break;
	case "cadastrar":
		TipoTituloController::cadastrar($PAdados);
		break;
	case "editar":
		TipoTituloController::editar($PAdados);
		break;
	case "atualizar":
		TipoTituloController::atualizar($PAdados);
		break;
	case "ativar":
		TipoTituloController::ativar($PAdados);
		break;
	case "inativar":
		TipoTituloController::inativar($PAdados);
		break;
	default:
		TipoTituloController::consultar($PAdados);
		break;
}

class TipoTituloController {
	
	public function consultar($PAdados) {
		$Atipotitulo = TipoTituloClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = TipoTituloClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objTipoTituloClass = new TipoTituloClass($PAdados);
		$objTipoTituloClass->edit();
		
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = TipoTituloClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = TipoTituloClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = TipoTituloClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}