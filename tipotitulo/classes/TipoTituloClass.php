<?php
include_once "tipotitulo/model/TipoTitulo.php";

class TipoTituloClass extends TipoTituloModel {
	
	public $id;
	public $nome;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->nome = $PAdados['nome'];
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Aartista = parent::editar($this->id);
		$this->id = $Aartista['id'];
		$this->nome = $Aartista['nome'];
		$this->cadastro = $Aartista['cadastro'];
		$this->status = $Aartista['status'];
	}
	
}