<?php
class TipoTituloModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, nome, cadastro, status
				FROM tipo_titulo";
		
		if($PAdados['filtro_ativo']) {
			$sql .= " WHERE status = 1 ";
		}
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['nome'] = $linha['nome'];
			$Adados[$i]['cadastro'] = $linha['cadastro'];
			$Adados[$i]['status'] = $linha['status'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO tipo_titulo 
				VALUES (
					NULL,
					'{$PAdados['nome']}',
					'{$PAdados['cadastro']}',
					{$PAdados['status']}
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, nome, cadastro, status
				FROM tipo_titulo
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE tipo_titulo 
				SET 
					nome = '{$PAdados['nome']}',
					status = {$PAdados['status']}
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function ativar($PAdados) {
		$sql = "UPDATE tipo_titulo SET status = 1 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function inativar($PAdados) {
		$sql = "UPDATE tipo_titulo SET status = 0 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}