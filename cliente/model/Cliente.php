<?php
class ClienteModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, nome, email, telefone, nascimento, cadastro, status
				FROM cliente";
		
		if($PAdados['filtro_ativo']) {
			$sql .= " WHERE status = 1 ";
		}
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['nome'] = $linha['nome'];
			$Adados[$i]['email'] = $linha['email'];
			$Adados[$i]['telefone'] = $linha['telefone'];
			$Adados[$i]['nascimento'] = $linha['nascimento'];
			$Adados[$i]['cadastro'] = $linha['cadastro'];
			$Adados[$i]['status'] = $linha['status'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO cliente 
				VALUES (
					NULL,
					'{$PAdados['nome']}',
					'{$PAdados['email']}',
					'{$PAdados['telefone']}',
					'{$PAdados['nascimento']}',
					'{$PAdados['cadastro']}',
					{$PAdados['status']}
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, nome, email, telefone, nascimento, cadastro, status
				FROM cliente
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE cliente 
				SET 
					nome = '{$PAdados['nome']}',
					email = '{$PAdados['email']}',
					telefone = '{$PAdados['telefone']}',
					nascimento = '{$PAdados['nascimento']}',
					status = {$PAdados['status']}
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function ativar($PAdados) {
		$sql = "UPDATE cliente SET status = 1 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function inativar($PAdados) {
		$sql = "UPDATE cliente SET status = 0 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}