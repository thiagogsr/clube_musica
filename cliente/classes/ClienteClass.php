<?php
include_once "cliente/model/Cliente.php";

class ClienteClass extends ClienteModel {
	
	public $id;
	public $nome;
	public $email;
	public $telefone;
	public $nascimento;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->nome = $PAdados['nome'];
			$this->email = $PAdados['email'];
			$this->telefone = Config::converterTelefone($PAdados['telefone']);
			$this->nascimento = Config::converterData($PAdados['nascimento']);
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Acliente = parent::editar($this->id);
		$this->id = $Acliente['id'];
		$this->nome = $Acliente['nome'];
		$this->email = $Acliente['email'];
		$this->telefone = $Acliente['telefone'];
		$this->nascimento = $Acliente['nascimento'];
		$this->cadastro = $Acliente['cadastro'];
		$this->status = $Acliente['status'];
	}
	
	public function cadastrar($PAdados) {
		$PAdados['telefone'] = Config::converterTelefone($PAdados['telefone']);
		$PAdados['nascimento'] = Config::converterData($PAdados['nascimento']);
		return parent::cadastrar($PAdados);
	}
	
	public function atualizar($PAdados) {
		$PAdados['telefone'] = Config::converterTelefone($PAdados['telefone']);
		$PAdados['nascimento'] = Config::converterData($PAdados['nascimento']);
		return parent::atualizar($PAdados);
	}
	
}