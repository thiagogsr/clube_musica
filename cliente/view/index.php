<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Clientes <span>(<a href="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/novo"; ?>" title="Novo">Novo</a>)</span></h2>
		
		<div class="mensagens">
			<?php 
			if($PAdados['acao'] == "ativar" && $PAdados['status']) { echo "<p>Ativado com sucesso</p>"; }
			if($PAdados['acao'] == "inativar" && $PAdados['status']) { echo "<p>Inativado com sucesso</p>"; }
			if($PAdados['acao'] == "cadastrar" && $PAdados['status']) { echo "<p>Cadastrado com sucesso</p>"; }
			if($PAdados['acao'] == "atualizar" && $PAdados['status']) { echo "<p>Atualizado com sucesso</p>"; }
			if($PAdados['acao'] == "ativar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao ativar</p>"; }
			if($PAdados['acao'] == "inativar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao inativar</p>"; }
			if($PAdados['acao'] == "cadastrar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao cadastrar</p>"; }
			if($PAdados['acao'] == "atualizar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao atualizar</p>"; }
			?>
		</div>
		
		<table width="100%">
			<thead>
				<tr>
					<th>Nome</th>
					<th>E-mail</th>
					<th>Telefone</th>
					<th>Nascimento</th>
					<th>Cadastro</th>
					<th>Ativo</th>
					<th class="center">Ação</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(!empty($Acliente)) {
					foreach ($Acliente as $linha) {
						echo "<tr>";
						echo "<td><a href='" . Config::$root . "/{$PAdados['modulo']}/editar/{$linha['id']}' title='Editar'>{$linha['nome']}</a></td>";
						echo "<td>{$linha['email']}</td>";
						echo "<td>" . Config::converterTelefone($linha['telefone'], 2) . "</td>";
						echo "<td>" . Config::converterData($linha['nascimento'], 2) . "</td>";
						echo "<td>" . Config::converterData($linha['cadastro'], 2) . "</td>";
						echo "<td>" . ($linha['status'] == 1 ? "Sim" : "Não") . "</td>";
						echo "<td align='center'>";
						if($linha['status'] == 1) {
							echo "<a href='" . Config::$root . "/{$PAdados['modulo']}/inativar/{$linha['id']}' title='Inativar' class='inativar'><span>Inativar</span></a>";
						} else {
							echo "<a href='" . Config::$root . "/{$PAdados['modulo']}/ativar/{$linha['id']}' title='Ativar' class='ativar'><span>Ativar</span></a>";
						}
						echo "</td>";
						echo "</tr>";
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>