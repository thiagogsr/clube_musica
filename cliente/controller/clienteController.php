<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "cliente/classes/ClienteClass.php";

switch ($PAdados['acao']) {
	case "novo":
		ClienteController::novo($PAdados);
		break;
	case "cadastrar":
		ClienteController::cadastrar($PAdados);
		break;
	case "editar":
		ClienteController::editar($PAdados);
		break;
	case "atualizar":
		ClienteController::atualizar($PAdados);
		break;
	case "ativar":
		ClienteController::ativar($PAdados);
		break;
	case "inativar":
		ClienteController::inativar($PAdados);
		break;
	default:
		ClienteController::consultar($PAdados);
		break;
}

class ClienteController {
	
	public function consultar($PAdados) {
		$Acliente = ClienteClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = ClienteClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objClienteClass = new ClienteClass($PAdados);
		$objClienteClass->edit();
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = ClienteClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = ClienteClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = ClienteClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}