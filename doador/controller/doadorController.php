<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "doador/classes/DoadorClass.php";

switch ($PAdados['acao']) {
	case "novo":
		DoadorController::novo($PAdados);
		break;
	case "cadastrar":
		DoadorController::cadastrar($PAdados);
		break;
	case "editar":
		DoadorController::editar($PAdados);
		break;
	case "atualizar":
		DoadorController::atualizar($PAdados);
		break;
	case "ativar":
		DoadorController::ativar($PAdados);
		break;
	case "inativar":
		DoadorController::inativar($PAdados);
		break;
	default:
		DoadorController::consultar($PAdados);
		break;
}

class DoadorController {
	
	public function consultar($PAdados) {
		$Adoador = DoadorClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = DoadorClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objDoadorClass = new DoadorClass($PAdados);
		$objDoadorClass->edit();
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = DoadorClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = DoadorClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = DoadorClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}