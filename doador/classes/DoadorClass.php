<?php
include_once "doador/model/Doador.php";

class DoadorClass extends DoadorModel {
	
	public $id;
	public $nome;
	public $email;
	public $telefone;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->nome = $PAdados['nome'];
			$this->email = $PAdados['email'];
			$this->telefone = Config::converterTelefone($PAdados['telefone']);
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Adoador = parent::editar($this->id);
		$this->id = $Adoador['id'];
		$this->nome = $Adoador['nome'];
		$this->email = $Adoador['email'];
		$this->telefone = $Adoador['telefone'];
		$this->cadastro = $Adoador['cadastro'];
		$this->status = $Adoador['status'];
	}
	
	public function cadastrar($PAdados) {
		$PAdados['telefone'] = Config::converterTelefone($PAdados['telefone']);
		return parent::cadastrar($PAdados);
	}
	
	public function atualizar($PAdados) {
		$PAdados['telefone'] = Config::converterTelefone($PAdados['telefone']);
		return parent::atualizar($PAdados);
	}
	
}