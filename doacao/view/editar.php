<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Editar Doação</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/atualizar/" . $PAdados['id']; ?>" method="post">
			<div>
				<label>Doador</label>
				<select name="doador_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Adoador)) {
						foreach ($Adoador as $linha) {
							$selected = $linha['id'] == $objDoacaoClass->doador_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Título</label>
				<select name="titulo_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Atitulo)) {
						foreach ($Atitulo as $linha) {
							$selected = $linha['id'] == $objDoacaoClass->titulo_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Quantidade</label>
				<input type="text" name="quantidade" value="<?php echo $objDoacaoClass->quantidade; ?>" />
			</div>
			
			<div>
				<label>Data</label>
				<input type="text" name="data" class="required data" value="<?php echo Config::converterData($objDoacaoClass->data, 2); ?>" />
			</div>
			
			<div class="submit">
				<input type="submit" value="Atualizar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>