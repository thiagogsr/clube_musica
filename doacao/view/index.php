<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Doações <span>(<a href="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/novo"; ?>" title="Novo">Novo</a>)</span></h2>
		
		<div class="mensagens">
			<?php
			if($PAdados['acao'] == "cadastrar" && $PAdados['status']) { echo "<p>Cadastrado com sucesso</p>"; }
			if($PAdados['acao'] == "atualizar" && $PAdados['status']) { echo "<p>Atualizado com sucesso</p>"; }
			if($PAdados['acao'] == "cadastrar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao cadastrar</p>"; }
			if($PAdados['acao'] == "atualizar" && !$PAdados['status']) { echo "<p class='erro'>Falha ao atualizar</p>"; }
			?>
		</div>
		
		<table width="100%">
			<thead>
				<tr>
					<th>Doador</th>
					<th>Título</th>
					<th>Quantidade</th>
					<th>Data</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(!empty($Adoacao)) {
					foreach ($Adoacao as $linha) {
						echo "<tr>";
						echo "<td><a href='" . Config::$root . "/{$PAdados['modulo']}/editar/{$linha['id']}' title='Editar'>{$linha['doador_nome']}</a></td>";
						echo "<td><a href='" . Config::$root . "/{$PAdados['modulo']}/editar/{$linha['id']}' title='Editar'>{$linha['titulo_nome']}</a></td>";
						echo "<td>{$linha['quantidade']}</td>";
						echo "<td>" . Config::converterData($linha['data'], 2) . "</td>";
						echo "</tr>";
					}
				}
				?>
			</tbody>
		</table>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>