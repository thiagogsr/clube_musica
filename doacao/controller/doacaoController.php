<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "doacao/classes/DoacaoClass.php";
include_once "doador/classes/DoadorClass.php";
include_once "titulo/classes/TituloClass.php";

switch ($PAdados['acao']) {
	case "novo":
		DoacaoController::novo($PAdados);
		break;
	case "cadastrar":
		DoacaoController::cadastrar($PAdados);
		break;
	case "editar":
		DoacaoController::editar($PAdados);
		break;
	case "atualizar":
		DoacaoController::atualizar($PAdados);
		break;
	default:
		DoacaoController::consultar($PAdados);
		break;
}

class DoacaoController {
	
	public function consultar($PAdados) {
		$Adoacao = DoacaoClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		$Adoador = DoadorClass::consultar($PAdados);
		$Atitulo = TituloClass::consultar($PAdados);
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		DoacaoClass::cadastrar($PAdados);
		
		$objTituloClass = new TituloClass(array("id" => $PAdados['titulo_id']));
		$objTituloClass->edit();
		$quantidade = $objTituloClass->quantidade + $PAdados['quantidade'];
		$PAdados['status'] = $objTituloClass->atualizarQuantidade(array("id" => $PAdados['titulo_id'], "quantidade" => $quantidade));
		
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objDoacaoClass = new DoacaoClass($PAdados);
		$objDoacaoClass->edit();
		
		$Adoador = DoadorClass::consultar($PAdados);
		$Atitulo = TituloClass::consultar($PAdados);
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$objDoacaoClass = new DoacaoClass($PAdados);
		$objDoacaoClass->edit();
		
		$objTituloClass = new TituloClass(array("id" => $PAdados['titulo_id']));
		$objTituloClass->edit();
		
		if($objDoacaoClass->quantidade < $PAdados['quantidade']) {
			$quantidade = $objTituloClass->quantidade + ($PAdados['quantidade'] - $objDoacaoClass->quantidade);
		} else if($objDoacaoClass->quantidade > $PAdados['quantidade']) {
			$quantidade = $objTituloClass->quantidade - ($objDoacaoClass->quantidade - $PAdados['quantidade']);
		} else {
			$quantidade = $objDoacaoClass->quantidade;
		}
		$objTituloClass->atualizarQuantidade(array("id" => $PAdados['titulo_id'], "quantidade" => $quantidade));
		
		$PAdados['status'] = DoacaoClass::atualizar($PAdados);
		
		self::consultar($PAdados);
	}

}