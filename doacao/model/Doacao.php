<?php
class DoacaoModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, doador_id, titulo_id, quantidade, data,
				(select nome from doador where id = doa.doador_id) AS doador_nome,
				(select nome from titulo where id = doa.titulo_id) AS titulo_nome
				FROM doacao doa";
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['doador_id'] = $linha['doador_id'];
			$Adados[$i]['doador_nome'] = $linha['doador_nome'];
			$Adados[$i]['titulo_id'] = $linha['titulo_id'];
			$Adados[$i]['titulo_nome'] = $linha['titulo_nome'];
			$Adados[$i]['quantidade'] = $linha['quantidade'];
			$Adados[$i]['data'] = $linha['data'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO doacao 
				VALUES (
					NULL,
					{$PAdados['doador_id']},
					{$PAdados['titulo_id']},
					{$PAdados['quantidade']},
					'{$PAdados['data']}'
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, doador_id, titulo_id, quantidade, data
				FROM doacao
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE doacao 
				SET 
					doador_id = {$PAdados['doador_id']},
					titulo_id = {$PAdados['titulo_id']},
					quantidade = {$PAdados['quantidade']},
					data = '{$PAdados['data']}'
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}