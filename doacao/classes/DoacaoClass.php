<?php
include_once "doacao/model/Doacao.php";

class DoacaoClass extends DoacaoModel {
	
	public $id;
	public $doador_id;
	public $titulo_id;
	public $quantidade;
	public $data;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->doador_id = $PAdados['doador_id'];
			$this->titulo_id = $PAdados['titulo_id'];
			$this->quantidade = $PAdados['quantidade'];
			$this->data = $PAdados['data'];
		}
	}
	
	public function edit() {
		$Adoacao = parent::editar($this->id);
		$this->id = $Adoacao['id'];
		$this->doador_id = $Adoacao['doador_id'];
		$this->titulo_id = $Adoacao['titulo_id'];
		$this->quantidade = $Adoacao['quantidade'];
		$this->data = $Adoacao['data'];
	}
	
	public function cadastrar($PAdados) {
		$PAdados['data'] = Config::converterData($PAdados['data']);
		return parent::cadastrar($PAdados);
	}
	
	public function atualizar($PAdados) {
		$PAdados['data'] = Config::converterData($PAdados['data']);
		return parent::atualizar($PAdados);
	}
	
}