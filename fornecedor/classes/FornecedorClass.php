<?php
include_once "fornecedor/model/Fornecedor.php";

class FornecedorClass extends FornecedorModel {
	
	public $id;
	public $nome;
	public $email;
	public $telefone;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->nome = $PAdados['nome'];
			$this->email = $PAdados['email'];
			$this->telefone = Config::converterTelefone($PAdados['telefone']);
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Afornecedor = parent::editar($this->id);
		$this->id = $Afornecedor['id'];
		$this->nome = $Afornecedor['nome'];
		$this->email = $Afornecedor['email'];
		$this->telefone = $Afornecedor['telefone'];
		$this->cadastro = $Afornecedor['cadastro'];
		$this->status = $Afornecedor['status'];
	}
	
	public function cadastrar($PAdados) {
		$PAdados['telefone'] = Config::converterTelefone($PAdados['telefone']);
		return parent::cadastrar($PAdados);
	}
	
	public function atualizar($PAdados) {
		$PAdados['telefone'] = Config::converterTelefone($PAdados['telefone']);
		return parent::atualizar($PAdados);
	}
	
}