<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "fornecedor/classes/FornecedorClass.php";

switch ($PAdados['acao']) {
	case "novo":
		FornecedorController::novo($PAdados);
		break;
	case "cadastrar":
		FornecedorController::cadastrar($PAdados);
		break;
	case "editar":
		FornecedorController::editar($PAdados);
		break;
	case "atualizar":
		FornecedorController::atualizar($PAdados);
		break;
	case "ativar":
		FornecedorController::ativar($PAdados);
		break;
	case "inativar":
		FornecedorController::inativar($PAdados);
		break;
	default:
		FornecedorController::consultar($PAdados);
		break;
}

class FornecedorController {
	
	public function consultar($PAdados) {
		$Afornecedor = FornecedorClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = FornecedorClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objFornecedorClass = new FornecedorClass($PAdados);
		$objFornecedorClass->edit();
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = FornecedorClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = FornecedorClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = FornecedorClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}