<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "titulo/classes/TituloClass.php";
include_once "tipotitulo/classes/TipoTituloClass.php";
include_once "artista/classes/ArtistaClass.php";

switch ($PAdados['acao']) {
	case "novo":
		TituloController::novo($PAdados);
		break;
	case "cadastrar":
		TituloController::cadastrar($PAdados);
		break;
	case "editar":
		TituloController::editar($PAdados);
		break;
	case "atualizar":
		TituloController::atualizar($PAdados);
		break;
	case "ativar":
		TituloController::ativar($PAdados);
		break;
	case "inativar":
		TituloController::inativar($PAdados);
		break;
	default:
		TituloController::consultar($PAdados);
		break;
}

class TituloController {
	
	public function consultar($PAdados) {
		$Atitulo = TituloClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		$Atipotitulo = TipoTituloClass::consultar(array("filtro_ativo" => true));
		$Aartista = ArtistaClass::consultar(array("filtro_ativo" => true));
		
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = TituloClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objTituloClass = new TituloClass($PAdados);
		$objTituloClass->edit();
		
		$Atipotitulo = TipoTituloClass::consultar(array("filtro_ativo" => true));
		$Aartista = ArtistaClass::consultar(array("filtro_ativo" => true));
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = TituloClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = TituloClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = TituloClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}