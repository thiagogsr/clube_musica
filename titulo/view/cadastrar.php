<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Cadastrar Título</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/cadastrar/"; ?>" method="post">
			<div>
				<label>Nome</label>
				<input type="text" name="nome" class="required" />
			</div>
			
			<div>
				<label>Tipo de título</label>
				<select name="tipo_titulo_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Atipotitulo)) {
						foreach ($Atipotitulo as $linha) {
							echo "<option value='{$linha['id']}'>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Artista</label>
				<select name="artista_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Aartista)) {
						foreach ($Aartista as $linha) {
							echo "<option value='{$linha['id']}'>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Valor</label>
				<input type="text" name="valor" class="required moeda" />
			</div>
			
			<div>
				<label>Quantidade</label>
				<input type="text" name="quantidade" class="required digits" />
			</div>
			
			<div>
				<label>Ativo</label>
				<select name="status">
					<option value="1" selected="selected">Sim</option>
					<option value="0">Não</option>
				</select>
			</div>
			
			<div class="submit">
				<input type="submit" value="Cadastrar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>