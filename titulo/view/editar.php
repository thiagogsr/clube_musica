<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Editar Título</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/atualizar/" . $PAdados['id']; ?>" method="post">
			<div>
				<label>Nome</label>
				<input type="text" name="nome" class="required" value="<?php echo $objTituloClass->nome; ?>" />
			</div>
			
			<div>
				<label>Tipo de título</label>
				<select name="tipo_titulo_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Atipotitulo)) {
						foreach ($Atipotitulo as $linha) {
							$selected = $linha['id'] == $objTituloClass->tipo_titulo_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Artista</label>
				<select name="artista_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Aartista)) {
						foreach ($Aartista as $linha) {
							$selected = $linha['id'] == $objTituloClass->artista_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Valor</label>
				<input type="text" name="valor" class="required moeda" value="<?php echo Config::converterMoeda($objTituloClass->valor, 2); ?>" />
			</div>
			
			<div>
				<label>Quantidade</label>
				<input type="text" name="quantidade" class="required digits" value="<?php echo $objTituloClass->quantidade; ?>" />
			</div>
			
			<div>
				<label>Ativo</label>
				<select name="status">
					<option value="1" selected="selected">Sim</option>
					<option value="0">Não</option>
				</select>
			</div>
			
			<div class="submit">
				<input type="submit" value="Atualizar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>