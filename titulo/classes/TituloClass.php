<?php
include_once "titulo/model/Titulo.php";

class TituloClass extends TituloModel {
	
	public $id;
	public $tipo_titulo_id;
	public $artista_id;
	public $nome;
	public $valor;
	public $quantidade;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->tipo_titulo_id = $PAdados['tipo_titulo_id'];
			$this->artista_id = $PAdados['artista_id'];
			$this->nome = $PAdados['nome'];
			$this->valor = Config::converterMoeda($PAdados['valor']);
			$this->quantidade = $PAdados['quantidade'];
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Aartista = parent::editar($this->id);
		$this->id = $Aartista['id'];
		$this->tipo_titulo_id = $Aartista['tipo_titulo_id'];
		$this->artista_id = $Aartista['artista_id'];
		$this->nome = $Aartista['nome'];
		$this->valor = $Aartista['valor'];
		$this->quantidade = $Aartista['quantidade'];
		$this->cadastro = $Aartista['cadastro'];
		$this->status = $Aartista['status'];	
	}
	
	public function cadastrar($PAdados) {
		$PAdados['valor'] = Config::converterMoeda($PAdados['valor']);
		return parent::cadastrar($PAdados);
	}
	
	public function atualizar($PAdados) {
		$PAdados['valor'] = Config::converterMoeda($PAdados['valor']);
		return parent::atualizar($PAdados);
	}
	
}