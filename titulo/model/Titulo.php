<?php
class TituloModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, tipo_titulo_id, artista_id, nome, valor, quantidade, cadastro, status,
				(select nome from tipo_titulo where id = tit.tipo_titulo_id) AS tipo_titulo_nome,
				(select nome from artista where id = tit.artista_id) AS artista_nome
				FROM titulo tit
				WHERE 1";
		
		if($PAdados['filtro_ativo']) {
			$sql .= " AND status = 1 ";
		}
		
		if($PAdados['filtro_quantidade']) {
			$sql .= " AND quantidade > 0 ";
		}
		
		$query = mysql_query($sql);
		$i = 0;
		$Agravadora = array();
		while ($linha = mysql_fetch_array($query)) {
			$Agravadora[$i]['id'] = $linha['id'];
			$Agravadora[$i]['tipo_titulo_id'] = $linha['tipo_titulo_id'];
			$Agravadora[$i]['artista_id'] = $linha['artista_id'];
			$Agravadora[$i]['tipo_titulo_nome'] = $linha['tipo_titulo_nome'];
			$Agravadora[$i]['artista_nome'] = $linha['artista_nome'];
			$Agravadora[$i]['nome'] = $linha['nome'];
			$Agravadora[$i]['valor'] = $linha['valor'];
			$Agravadora[$i]['quantidade'] = $linha['quantidade'];
			$Agravadora[$i]['cadastro'] = $linha['cadastro'];
			$Agravadora[$i]['status'] = $linha['status'];
			$i++; 
		}
		
		return $Agravadora;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO titulo 
				VALUES (
					NULL,
					{$PAdados['tipo_titulo_id']},
					{$PAdados['artista_id']},
					'{$PAdados['nome']}',
					{$PAdados['valor']},
					{$PAdados['quantidade']},
					'{$PAdados['cadastro']}',
					{$PAdados['status']}
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, tipo_titulo_id, artista_id, nome, valor, quantidade, cadastro, status
				FROM titulo
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE titulo 
				SET 
					tipo_titulo_id = {$PAdados['tipo_titulo_id']},
					artista_id = {$PAdados['artista_id']},
					nome = '{$PAdados['nome']}',
					valor = {$PAdados['valor']},
					quantidade = {$PAdados['quantidade']},
					status = {$PAdados['status']}
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function ativar($PAdados) {
		$sql = "UPDATE titulo SET status = 1 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function inativar($PAdados) {
		$sql = "UPDATE titulo SET status = 0 WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
	public function atualizarQuantidade($PAdados) {
		$sql = "UPDATE titulo SET quantidade = {$PAdados['quantidade']} WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}