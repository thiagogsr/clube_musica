<?php
include_once "configuration.php";

class ConexaoClass {

	/**
	 * __construct():
	 * Método para conectar base de dados com tratamento de excessão;
	 * @see Config
	 */
	function __construct() {
		try {
			if(mysql_connect(Config::$host, Config::$user, Config::$password)) {
				if(mysql_select_db(Config::$database)) {
					mysql_set_charset('utf8');
				} else {
					throw new Exception(mysql_error());
				}
			} else {
				throw new Exception(mysql_error());
			}
			
		} catch (Exception $e) {
			echo "<p class='error-message'>Erro ao conectar banco de dados: " . $e->getMessage() . "</p>";
		}
	}
	
}

new ConexaoClass();