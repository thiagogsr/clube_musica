<?php
class LocacaoItemModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, locacao_id, titulo_id, valor, data_devolucao, data_devolvido, multa
				FROM locacao_item
				WHERE locacao_id = " . (int) $PAdados['id'];
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['locacao_id'] = $linha['locacao_id'];
			$Adados[$i]['titulo_id'] = $linha['titulo_id'];
			$Adados[$i]['valor'] = $linha['valor'];
			$Adados[$i]['data_devolucao'] = $linha['data_devolucao'];
			$Adados[$i]['data_devolvido'] = $linha['data_devolvido'];
			$Adados[$i]['multa'] = $linha['multa'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO locacao_item 
				VALUES (
					NULL,
					{$PAdados['locacao_id']},
					{$PAdados['titulo_id']},
					{$PAdados['valor']},
					'{$PAdados['data_devolucao']}',
					" . ($PAdados['data_devolvido'] ? "'{$PAdados['data_devolvido']}'" : "NULL") . ",
					{$PAdados['multa']}
				)";

		return (int) mysql_query($sql);
	}
	
	public function apagar($PAdados) {
		$sql = "DELETE FROM locacao_item WHERE locacao_id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}