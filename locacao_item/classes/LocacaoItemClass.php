<?php
include_once "locacao_item/model/LocacaoItem.php";


class LocacaoItemClass extends LocacaoItemModel {
	
	public $id;
	public $locacao_id;
	public $titulo_id;
	public $valor;
	public $data_devolucao;
	public $data_devolvido;
	public $multa;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->locacao_id = $PAdados['locacao_id'];
			$this->titulo_id = $PAdados['titulo_id'];
			$this->valor = Config::converterMoeda($PAdados['valor']);
			$this->data_devolucao = Config::converterData($PAdados['data_devolucao']);
			$this->data_devolvido = Config::converterData($PAdados['data_devolvido']);
			$this->multa = Config::converterMoeda($PAdados['multa']);
		}
	}
	
	public function cadastrar($PAdados) {
		$PAdados['valor'] = Config::converterMoeda($PAdados['valor']);
		$PAdados['data_devolucao'] = Config::converterData($PAdados['data_devolucao']);
		$PAdados['multa'] = Config::converterMoeda($PAdados['multa']);
		$PAdados['data_devolvido'] = $PAdados['data_devolvido'] ? Config::converterData($PAdados['data_devolvido']) : NULL;
		
		return parent::cadastrar($PAdados);
	}
	
}