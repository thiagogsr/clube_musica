SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE `artista` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gravadora_id` int(10) unsigned NOT NULL,
  `nome` varchar(100) NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `gravadora_id` (`gravadora_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `artista` VALUES(2, 10, 'Rojão Diferente', '2012-04-15', 1);

CREATE TABLE `cliente` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  `nascimento` date NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

INSERT INTO `cliente` VALUES(2, 'Thiago Guimarães', 'thiagogsr@gmail.com', '7999628793', '1991-08-15', '2012-04-15', 1);
INSERT INTO `cliente` VALUES(4, 'Thiago Guimarães Santa Rosa', 'thiago@gmail.com', '7999628793', '1991-08-15', '2012-04-15', 1);

CREATE TABLE `compra` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fornecedor_id` int(10) unsigned NOT NULL,
  `titulo_id` int(10) unsigned NOT NULL,
  `valor` decimal(12,2) unsigned NOT NULL,
  `quantidade` tinyint(3) unsigned NOT NULL,
  `cadastro` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fornecedor_id` (`fornecedor_id`),
  KEY `titulo_id` (`titulo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `compra` VALUES(1, 1, 1, 10.00, 3, '2012-04-16');

CREATE TABLE `doacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `doador_id` int(10) unsigned NOT NULL,
  `titulo_id` int(10) unsigned NOT NULL,
  `quantidade` tinyint(3) unsigned NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doador_id` (`doador_id`),
  KEY `titulo_id` (`titulo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `doacao` VALUES(1, 1, 1, 2, '2012-04-16');

CREATE TABLE `doador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

CREATE TABLE `fornecedor` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telefone` varchar(10) NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `fornecedor` VALUES(1, 'Thiago Guimarães', 'thiagogsr@gmail.com', '7932512209', '2012-04-15', 1);

CREATE TABLE `gravadora` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

INSERT INTO `gravadora` VALUES(9, 'Sony', '2012-04-15', 0);
INSERT INTO `gravadora` VALUES(10, 'BMG', '2012-04-15', 1);

CREATE TABLE `locacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cliente_id` int(10) unsigned NOT NULL,
  `data` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cliente_id` (`cliente_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

INSERT INTO `locacao` VALUES(20, 2, '2012-04-16');
INSERT INTO `locacao` VALUES(21, 4, '2012-04-16');
INSERT INTO `locacao` VALUES(22, 4, '2012-04-16');

CREATE TABLE `locacao_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `locacao_id` int(10) unsigned NOT NULL,
  `titulo_id` int(10) unsigned NOT NULL,
  `valor` decimal(12,2) unsigned NOT NULL,
  `data_devolucao` date NOT NULL,
  `data_devolvido` date DEFAULT NULL,
  `multa` decimal(12,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locacao_id` (`locacao_id`),
  KEY `titulo_id` (`titulo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

INSERT INTO `locacao_item` VALUES(1, 20, 1, 100.00, '2012-04-10', NULL, 10.00);
INSERT INTO `locacao_item` VALUES(2, 21, 1, 200.00, '2012-04-26', NULL, 2.00);
INSERT INTO `locacao_item` VALUES(3, 22, 1, 20.00, '2012-04-28', NULL, 1.10);

CREATE TABLE `tipo_titulo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

INSERT INTO `tipo_titulo` VALUES(1, 'Cds', '2012-04-15', 1);
INSERT INTO `tipo_titulo` VALUES(2, 'Dvds', '2012-04-15', 1);

CREATE TABLE `titulo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo_titulo_id` int(10) unsigned NOT NULL,
  `artista_id` int(10) unsigned NOT NULL,
  `nome` varchar(100) NOT NULL,
  `valor` decimal(12,2) unsigned NOT NULL,
  `quantidade` tinyint(3) unsigned NOT NULL,
  `cadastro` date NOT NULL,
  `status` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `artista_id` (`artista_id`),
  KEY `tipo_titulo_id` (`tipo_titulo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `titulo` VALUES(1, 2, 2, 'Ao Vivo', 100.00, 2, '2012-04-15', 1);


ALTER TABLE `artista`
  ADD CONSTRAINT `artista_ibfk_1` FOREIGN KEY (`gravadora_id`) REFERENCES `gravadora` (`id`);

ALTER TABLE `compra`
  ADD CONSTRAINT `compra_ibfk_2` FOREIGN KEY (`titulo_id`) REFERENCES `titulo` (`id`),
  ADD CONSTRAINT `compra_ibfk_1` FOREIGN KEY (`fornecedor_id`) REFERENCES `fornecedor` (`id`);

ALTER TABLE `doacao`
  ADD CONSTRAINT `doacao_ibfk_1` FOREIGN KEY (`doador_id`) REFERENCES `doador` (`id`),
  ADD CONSTRAINT `doacao_ibfk_2` FOREIGN KEY (`titulo_id`) REFERENCES `titulo` (`id`);

ALTER TABLE `locacao`
  ADD CONSTRAINT `locacao_ibfk_1` FOREIGN KEY (`cliente_id`) REFERENCES `cliente` (`id`);

ALTER TABLE `locacao_item`
  ADD CONSTRAINT `locacao_item_ibfk_1` FOREIGN KEY (`locacao_id`) REFERENCES `locacao` (`id`),
  ADD CONSTRAINT `locacao_item_ibfk_2` FOREIGN KEY (`titulo_id`) REFERENCES `titulo` (`id`);

ALTER TABLE `titulo`
  ADD CONSTRAINT `titulo_ibfk_2` FOREIGN KEY (`artista_id`) REFERENCES `artista` (`id`),
  ADD CONSTRAINT `titulo_ibfk_1` FOREIGN KEY (`tipo_titulo_id`) REFERENCES `tipo_titulo` (`id`);
