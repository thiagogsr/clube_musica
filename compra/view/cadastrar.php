<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Cadastrar Compra</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/cadastrar/"; ?>" method="post">
			
			<div>
				<label>Fornecedor</label>
				<select name="fornecedor_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Afornecedor)) {
						foreach ($Afornecedor as $linha) {
							echo "<option value='{$linha['id']}'>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Título</label>
				<select name="titulo_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Atitulo)) {
						foreach ($Atitulo as $linha) {
							echo "<option value='{$linha['id']}'>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Valor</label>
				<input type="text" name="valor" class="required moeda" />
			</div>
			
			<div>
				<label>Quantidade</label>
				<input type="text" name="quantidade" class="required digits" />
			</div>
			
			<div class="submit">
				<input type="submit" value="Cadastrar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>