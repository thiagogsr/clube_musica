<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Editar Compra</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/atualizar/" . $PAdados['id']; ?>" method="post">
			<div>
				<label>Fornecedor</label>
				<select name="fornecedor_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Afornecedor)) {
						foreach ($Afornecedor as $linha) {
							$selected = $linha['id'] == $objCompraClass->fornecedor_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Título</label>
				<select name="titulo_id" class="required">
					<option value="">Selecione uma opção</option>
					<?php 
					if(!empty($Atitulo)) {
						foreach ($Atitulo as $linha) {
							$selected = $linha['id'] == $objCompraClass->titulo_id ? "selected='selected'" : "";
							echo "<option value='{$linha['id']}' $selected>{$linha['nome']}</option>";
						}
					}
					?>
				</select>
			</div>
			
			<div>
				<label>Valor</label>
				<input type="text" name="valor" class="required moeda" value="<?php echo Config::converterMoeda($objCompraClass->valor); ?>" />
			</div>
			
			<div>
				<label>Quantidade</label>
				<input type="text" name="quantidade" class="required digits" value="<?php echo $objCompraClass->quantidade; ?>" />
			</div>
			
			<div class="submit">
				<input type="submit" value="Atualizar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>