<?php
class CompraModel {
	
	public function consultar($PAdados) {
		$sql = "SELECT id, fornecedor_id, titulo_id, valor, quantidade, cadastro,
				(select nome from fornecedor where id = com.fornecedor_id) AS fornecedor_nome,
				(select nome from titulo where id = com.titulo_id) AS titulo_nome
				FROM compra com";
		
		$query = mysql_query($sql);
		$i = 0;
		$Adados = array();
		while ($linha = mysql_fetch_array($query)) {
			$Adados[$i]['id'] = $linha['id'];
			$Adados[$i]['fornecedor_id'] = $linha['fornecedor_id'];
			$Adados[$i]['fornecedor_nome'] = $linha['fornecedor_nome'];
			$Adados[$i]['titulo_id'] = $linha['titulo_id'];
			$Adados[$i]['titulo_nome'] = $linha['titulo_nome'];
			$Adados[$i]['valor'] = $linha['valor'];
			$Adados[$i]['quantidade'] = $linha['quantidade'];
			$Adados[$i]['cadastro'] = $linha['cadastro'];
			$i++; 
		}
		
		return $Adados;
	}
	
	public function cadastrar($PAdados) {
		$sql = "INSERT INTO compra 
				VALUES (
					NULL,
					{$PAdados['fornecedor_id']},
					{$PAdados['titulo_id']},
					{$PAdados['valor']},
					{$PAdados['quantidade']},
					'{$PAdados['cadastro']}'
				)";
		
		return (int) mysql_query($sql);
	}
	
	public function editar($Pid) {
		$sql = "SELECT id, fornecedor_id, titulo_id, valor, quantidade, cadastro
				FROM compra
				WHERE id = " . (int) $Pid;
		
		$query = mysql_query($sql);
		return mysql_fetch_array($query);
	}
	
	public function atualizar($PAdados) {
		$sql = "UPDATE compra 
				SET 
					fornecedor_id = {$PAdados['fornecedor_id']},
					titulo_id = {$PAdados['titulo_id']},
					valor = {$PAdados['valor']},
					quantidade = {$PAdados['quantidade']}
				WHERE id = " . (int) $PAdados['id'];
		
		return (int) mysql_query($sql);
	}
	
}