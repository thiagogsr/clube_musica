<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "compra/classes/CompraClass.php";
include_once "fornecedor/classes/FornecedorClass.php";
include_once "titulo/classes/TituloClass.php";

switch ($PAdados['acao']) {
	case "novo":
		CompraController::novo($PAdados);
		break;
	case "cadastrar":
		CompraController::cadastrar($PAdados);
		break;
	case "editar":
		CompraController::editar($PAdados);
		break;
	case "atualizar":
		CompraController::atualizar($PAdados);
		break;
	default:
		CompraController::consultar($PAdados);
		break;
}

class CompraController {
	
	public function consultar($PAdados) {
		$Acompra = CompraClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		$Afornecedor = FornecedorClass::consultar(array("filtro_ativo" => true));
		$Atitulo = TituloClass::consultar(array("filtro_ativo" => true));
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		CompraClass::cadastrar($PAdados);
		
		$objTituloClass = new TituloClass(array("id" => $PAdados['titulo_id']));
		$objTituloClass->edit();
		$quantidade = $objTituloClass->quantidade + $PAdados['quantidade'];
		$PAdados['status'] = $objTituloClass->atualizarQuantidade(array("id" => $PAdados['titulo_id'], "quantidade" => $quantidade));
		
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objCompraClass = new CompraClass($PAdados);
		$objCompraClass->edit();
		
		$Afornecedor = FornecedorClass::consultar(array("filtro_ativo" => true));
		$Atitulo = TituloClass::consultar(array("filtro_ativo" => true));
		
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$objCompraClass = new CompraClass($PAdados);
		$objCompraClass->edit();
		
		$objTituloClass = new TituloClass(array("id" => $PAdados['titulo_id']));
		$objTituloClass->edit();
		
		if($objCompraClass->quantidade < $PAdados['quantidade']) {
			$quantidade = $objTituloClass->quantidade + ($PAdados['quantidade'] - $objCompraClass->quantidade);
		} else if($objCompraClass->quantidade > $PAdados['quantidade']) {
			$quantidade = $objTituloClass->quantidade - ($objCompraClass->quantidade - $PAdados['quantidade']);
		} else {
			$quantidade = $objCompraClass->quantidade;
		}
		$objTituloClass->atualizarQuantidade(array("id" => $PAdados['titulo_id'], "quantidade" => $quantidade));
		
		$PAdados['status'] = CompraClass::atualizar($PAdados);
		self::consultar($PAdados);
	}

}