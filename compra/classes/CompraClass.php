<?php
include_once "compra/model/Compra.php";

class CompraClass extends CompraModel {
	
	public $id;
	public $fornecedor_id;
	public $titulo_id;
	public $valor;
	public $quantidade;
	public $cadastro;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->fornecedor_id = $PAdados['fornecedor_id'];
			$this->titulo_id = $PAdados['titulo_id'];
			$this->valor = Config::converterMoeda($PAdados['valor']);
			$this->quantidade = $PAdados['quantidade'];
			$this->cadastro = $PAdados['cadastro'];
		}
	}
	
	public function edit() {
		$Acompra = parent::editar($this->id);
		$this->id = $Acompra['id'];
		$this->fornecedor_id = $Acompra['fornecedor_id'];
		$this->titulo_id = $Acompra['titulo_id'];
		$this->valor = $Acompra['valor'];
		$this->quantidade = $Acompra['quantidade'];
		$this->cadastro = $Acompra['cadastro'];
	}
	
	public function cadastrar($PAdados) {
		$PAdados['valor'] = Config::converterMoeda($PAdados['valor']);
		return parent::cadastrar($PAdados);
	}
	
	public function atualizar($PAdados) {
		$PAdados['valor'] = Config::converterMoeda($PAdados['valor']);
		return parent::atualizar($PAdados);
	}
	
}