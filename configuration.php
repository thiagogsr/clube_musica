<?php
ini_set('display_errors', false);
$PAdados = Config::getData();

function e() {
	ini_set('display_errors', true);
}

function print_pre($Parray) {
	echo "<pre>"; print_r($Parray); echo "</pre>";
}

class Config {

	static $host = "";
	static $database = "";
	static $user = "";
	static $password = "";
	static $default = "locacao/view/index.php";
	static $root = "/clientes/clube";

	public function getData() {
		$Data = array();
		foreach ($_GET as $postition => $data) {
			if (!is_null($data) && $data != "") {
				$Data [$postition] = $data;
			}
		}
		foreach ($_POST as $postition => $data) {
			if(!is_null($data)) {
				if ($postition == "acao") {
					$Data[$postition] = str_replace(" ", "", $data);
				} else {
					$Data[$postition] = $data;
				}
			}
		}
		return $Data;
	}

	public function converterData($Pdata, $Ptipo = 1) {
		switch ($Ptipo) {
			case 1:
				return implode("-", array_reverse(explode("/", $Pdata)));
				break;
			case 2:
				return date("d/m/Y", strtotime($Pdata));
				break;
		}
	}

	public function converterMoeda($Pvalor, $Ptipo = 1) {
		switch ($Ptipo) {
			case 1:
				return str_replace(",", ".", str_replace(array(".", "R$ "), "", $Pvalor));
				break;
			case 2:
				return number_format($Pvalor, 2, ",", ".");
				break;
		}
	}

	public function converterTelefone($Ptelefone, $Ptipo = 1) {
		switch ($Ptipo) {
			case 1:
				return str_replace(array("(", ")", "-", " "), "", $Ptelefone);
				break;
			case 2:
				return "(" . substr($Ptelefone, 0, 2) . ") " . substr($Ptelefone, 2, 4) . "-" . substr($Ptelefone, 6, 4);
				break;
		}
	}

}