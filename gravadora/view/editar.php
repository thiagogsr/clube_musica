<?php include_once "comum/view/header.php"; ?>

<div id="content">
	<div class="wrap">
		<h2>Editar Gravadora</h2>
		
		<form class="form" action="<?php echo Config::$root . "/" . $PAdados['modulo'] . "/atualizar/" . $PAdados['id']; ?>" method="post">
			<div>
				<label>Nome</label>
				<input type="text" name="nome" class="required" value="<?php echo $objGravadoraClass->nome; ?>" />
			</div>
			
			<div>
				<label>Ativo</label>
				<select name="status">
					<option value="1" <?php echo $objGravadoraClass->status == 1 ? "selected='selected'" : ""; ?>>Sim</option>
					<option value="0" <?php echo $objGravadoraClass->status == 0 ? "selected='selected'" : ""; ?>>Não</option>
				</select>
			</div>
			
			<div class="submit">
				<input type="submit" value="Atualizar" />
			</div>
		</form>
	</div>
</div>

<?php include_once "comum/view/footer.php"; ?>