<?php
include_once "conexao/classes/ConexaoClass.php";
include_once "gravadora/classes/GravadoraClass.php";

switch ($PAdados['acao']) {
	case "novo":
		GravadoraController::novo($PAdados);
		break;
	case "cadastrar":
		GravadoraController::cadastrar($PAdados);
		break;
	case "editar":
		GravadoraController::editar($PAdados);
		break;
	case "atualizar":
		GravadoraController::atualizar($PAdados);
		break;
	case "ativar":
		GravadoraController::ativar($PAdados);
		break;
	case "inativar":
		GravadoraController::inativar($PAdados);
		break;
	default:
		GravadoraController::consultar($PAdados);
		break;
}

class GravadoraController {
	
	public function consultar($PAdados) {
		$Agravadora = GravadoraClass::consultar($PAdados);
		include_once "../view/index.php";
	}
	
	public function novo($PAdados) {
		include_once "../view/cadastrar.php";
	}
	
	public function cadastrar($PAdados) {
		$PAdados['cadastro'] = date("Y-m-d");
		$PAdados['status'] = GravadoraClass::cadastrar($PAdados);
		self::consultar($PAdados);
	}
	
	public function editar($PAdados) {
		$objGravadoraClass = new GravadoraClass($PAdados);
		$objGravadoraClass->edit();
		
		include_once "../view/editar.php";
	}
	
	public function atualizar($PAdados) {
		$PAdados['status'] = GravadoraClass::atualizar($PAdados);
		self::consultar($PAdados);
	}
	
	public function ativar($PAdados) {
		$PAdados['status'] = GravadoraClass::ativar($PAdados);
		self::consultar($PAdados);
	}
	
	public function inativar($PAdados) {
		$PAdados['status'] = GravadoraClass::inativar($PAdados);
		self::consultar($PAdados);
	}

}