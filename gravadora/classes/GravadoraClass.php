<?php
include_once "gravadora/model/Gravadora.php";

class GravadoraClass extends GravadoraModel {
	
	public $id;
	public $nome;
	public $cadastro;
	public $status;
	
	function __construct($PAdados = null) {
		if(!empty($PAdados) && is_array($PAdados)) {
			$this->id = $PAdados['id'];
			$this->nome = $PAdados['nome'];
			$this->cadastro = Config::converterData($PAdados['cadastro']);
			$this->status = $PAdados['status'];	
		}
	}
	
	public function edit() {
		$Aartista = parent::editar($this->id);
		$this->id = $Aartista['id'];
		$this->nome = $Aartista['nome'];
		$this->cadastro = $Aartista['cadastro'];
		$this->status = $Aartista['status'];
	}
	
}